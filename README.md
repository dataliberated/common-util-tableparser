# common-tableparser (WIP)

### Utility for parsing html tables to various other formats.

#### Currently supports javascript array, CSV and TSV export through toArray(), toCSV() and toTSV() respectively.
