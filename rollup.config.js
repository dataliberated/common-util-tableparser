import { terser } from "rollup-plugin-terser";

export default {

	input: "./src/js/main.js",
	output: {
		name: "common.util.tableparser",
		file: "./dist/common-util-tableparser.min.js",
		format: "iife",
		sourceMap: true
	},
	plugins: [
		terser()
	]

};
