// Pollyfills for string trimming.
if ( window.String.prototype.trimLeft === undefined ) {
	window.String.prototype.trimLeft = function() {
		return this.replace( /^\s*/, "" );
	}
}

if ( window.String.prototype.trimRight === undefined ) {
	window.String.prototype.trimRight = function() {
		return this.replace( /\s*$/, "" );
	}
}

export function TableParser() {

	var _dataArray = [];

	this.parseTable = function( tableElement, stripHTML, columns ) {

		stripHTML = stripHTML !== undefined ? stripHTML : true;

		if (
			!( tableElement instanceof HTMLElement ) ||
			(
				tableElement.tagName !== "TABLE" &&
				tableElement.tagName !== "THEAD" &&
				tableElement.tagName !== "TBODY" &&
				tableElement.tagName !== "TFOOT"
			)
		) {
			throw ( "Target to parse is not a HTML DOM table element." );
		}

		var parseChild = function( child ) {
			var children = child.childNodes;
			for ( var i = 0; i < children.length; i++ ) {
				switch ( children[ i ].tagName ) {
					case "TABLE":
						throw ( "Nested tables are not supported." );
					case "THEAD":
						parseChild( children[ i ] );
						break;
					case "TBODY":
						parseChild( children[ i ] );
						break;
					case "TFOOT":
						parseChild( children[ i ] );
						break;
					case "TR":
						var tdChildren = children[ i ].getElementsByTagName( "TD" );
						tdChildren = [].concat.apply( [], tdChildren );
						var thChildren = children[ i ].getElementsByTagName( "TH" );
						thChildren = [].concat.apply( [], thChildren );
						var rowChildren = tdChildren.concat( thChildren );
						var parsedRow = [];
						for ( var j = 0; j < rowChildren.length; j++ ) {
							if ( columnIncluded( j ) ) {
								if ( stripHTML ) {
									var contents = rowChildren[ j ].innerText ||
										rowChildren[ j ].textContent;
									parsedRow[ parsedRow.length ] = contents
										.replace( /[\n\r]/g, "" )
										.trimLeft()
										.trimRight();
								} else {
									parsedRow[ parsedRow.length ] = rowChildren[ j ].innerHTML
										.replace( /[\n\r]/g, "" )
										.trimLeft()
										.trimRight();
								}
							}

						}
						_dataArray[ _dataArray.length ] = parsedRow;
						break;
				}
			}
		};

		var columnIncluded = function( columnIndex ) {
			if ( columns === undefined ) {
				return true;
			} else {
				for ( var column = 0; column < columns.length; column++ ) {
					if ( columnIndex === columns[ column ] ) {
						return true;
					}
				}
				return false;
			}
		};

		parseChild( tableElement );

		return this;
	};

	this.toArray = function() {
		return _dataArray;
	};

	this.toCSV = function() {
		var csvString = "";

		for ( var i = 0; i < _dataArray.length; i++ ) {
			csvString += _dataArray[ i ].join( "," ) + "\n";
		}

		return csvString;
	};

	this.toTSV = function() {
		var tsvString = "";

		for ( var i = 0; i < _dataArray.length; i++ ) {
			tsvString += _dataArray[ i ].join( "\t" ) + "\n";
		}

		return tsvString;
	};

}
